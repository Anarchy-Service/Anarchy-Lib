<h1 align="center">    
  <img src="https://camo.githubusercontent.com/259cdafa5ebb5fe3583595ffa2f76ccd1e6188df/687474703a2f2f7331322e7069636f66696c652e636f6d2f66696c652f383430333639373536382f352e6a7067">
   <br>
</h1>
  
# Anarchy Group Manager Library V2 Pre-Alpha
 [![API Version](https://img.shields.io/badge/Bot%20API-4.9%20%28June%202020%29-32a2da.svg)](https://core.telegram.org/bots/api#june-4-2020)
 [![GitHub license](https://img.shields.io/github/license/persepolisdm/persepolis.svg)](https://github.com/Anarchy-Service/Anarchy-Bot/blob/master/LICENSE)
 [![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D7.4-8892BF.svg)](https://php.net/)
 [![Code Quality](https://scrutinizer-ci.com/g/Anarchy-Service/Anarchy-Bot/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/Anarchy-Service/Anarchy-Bot/?branch=master/)
 [![Build](https://scrutinizer-ci.com/g/Anarchy-Service/Anarchy-Bot/badges/build.png?b=master)](https://scrutinizer-ci.com/g/Anarchy-Service/Anarchy-Bot/?branch=master)
 [![Code Intelligence](https://scrutinizer-ci.com/g/Anarchy-Service/Anarchy-Bot/badges/code-intelligence.svg?b=master)](https://scrutinizer-ci.com/g/Anarchy-Service/Anarchy-Bot/?branch=master)
 
Easy Use Anarchy Bot on Telegram Groups
 <p>
  <br>
<b>How To Use Anarchy Bot on Groups </b>

- Just Add [@AnarchyService_Bot](https://telegram.me/AnarchyService_Bot) On Your Group :)
<br>
 <p>
<b>How To Use Anarchy Bot Source </b>

- Run Anarchy Service Composer Install

```php
composer create-project anarchyservice/anarchy-bot
```
- You're env.php file should be something like env.php.sample, then you gotta set a webhook on hook.php
     
<br>




<b>Bot :</b> [Anarchy Bot](https://github.com/Anarchy-Service/Anarchy-Bot)

<b>Telegram :</b> [@AnarchyService_Bot](https://telegram.me/AnarchyService_Bot)
